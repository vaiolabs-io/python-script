
---
# Python Script Parameters

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# Output and Input  
<!-- par.py -->
---
# Positional parameters - sys module

---
# sys.stdout.write

---
# sys.stdin.read   /  readline   / readlines

----
# Argument parameters - argparse module
