
---
# Python Script Network Modules

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# SOCKET
Sockets have a long history. Their use originated with ARPANET in 1971 and later became an API in the Berkeley Software Distribution (BSD) operating system released in 1983 called Berkeley sockets.

When the Internet took off in the 1990s with the World Wide Web, so did network programming. Web servers and browsers weren’t the only applications taking advantage of newly connected networks and using sockets. Client-server applications of all types and sizes came into widespread use.

Today, although the underlying protocols used by the socket API have evolved over the years, and new ones have developed, the low-level API has remained the same.

The most common type of socket applications are client-server applications, where one side acts as the server and waits for connections from clients. This is the type of application that you’ll be creating in this tutorial. More specifically, you’ll focus on the socket API for Internet sockets, sometimes called Berkeley or BSD sockets. There are also Unix domain sockets, which can only be used to communicate between processes on the same host.

## Overview

Python’s socket module provides an interface to the Berkeley sockets API. This is the module that you’ll use in this tutorial.

The primary socket API functions and methods in this module are:

- socket()
- .bind()
- .listen()
- .accept()
- .connect()
- .connect_ex()
- .send()
- .recv()
- .close()

Python provides a convenient and consistent API that maps directly to system calls, their C counterparts. In the next section, you’ll learn how these are used together.
---
# IPADDRESS/Tabulate

`ipaddress` library enables us to create ipv4 or ipv6 addresses
It is mostly used for checking ip address configurations natively with python.

```py
import ipaddress

ip1 = ipaddress.ip_address
```

---
# JSON

---

# YAML


---
# XMLTODICT


---

# PEXPECT


---

# Telnetlib


---

# PARAMIKO

---

# NETMIKO

---

